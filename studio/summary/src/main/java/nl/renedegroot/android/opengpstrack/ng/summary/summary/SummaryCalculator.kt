/*
 * Open GPS Tracker
 * Copyright (C) 2018  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.renedegroot.android.opengpstrack.ng.summary.summary

import android.content.Context
import android.location.Location
import android.net.Uri
import nl.renedegroot.android.gpstracker.service.integration.ContentConstants
import nl.renedegroot.android.gpstracker.service.util.DefaultResultHandler
import nl.renedegroot.android.gpstracker.service.util.Waypoint
import nl.renedegroot.android.gpstracker.service.util.readTrack
import nl.renedegroot.android.gpstracker.service.util.readTrackType
import nl.renedegroot.android.gpstracker.utils.contentprovider.getString
import nl.renedegroot.android.gpstracker.utils.contentprovider.runQuery

class SummaryCalculator(val context: Context) {

    fun calculateSummary(trackUri: Uri): Summary {
        // Calculate
        val handler = DefaultResultHandler()
        trackUri.readTrack(handler)
        val startTimestamp = handler.waypoints.firstOrNull()?.firstOrNull()?.time ?: 0L
        val endTimestamp = handler.waypoints.lastOrNull()?.lastOrNull()?.time ?: 0L

        val outArray = floatArrayOf(0.0F)
        var totalDistance = 0F
        var maxMeterPerMilliSecondSpeed = 0F
        val deltas = handler.waypoints.map {
            it.mapIndexed { index, rhs ->
                val lhs = if (index > 0) it[index - 1] else rhs
                val meters = distance(lhs, rhs, outArray)
                totalDistance += meters
                val milliseconds = rhs.time - lhs.time
                (meters / milliseconds).let { currentMillisecondSpeed ->
                    if (currentMillisecondSpeed > maxMeterPerMilliSecondSpeed) {
                        maxMeterPerMilliSecondSpeed = currentMillisecondSpeed
                    }
                }
                Summary.Delta(rhs.time, totalDistance, meters, milliseconds, rhs.altitude)
            }
        }
        var trackedPeriod = 0L
        deltas.forEach {
            it.forEach { delta ->
                trackedPeriod += delta.deltaMilliseconds
            }
        }

        // Text values
        val name = trackUri.runQuery(context.contentResolver) { it.getString(ContentConstants.Tracks.NAME) }
                ?: "Unknown"
        val trackType = trackUri.readTrackType()

        // Return value

        return Summary(trackUri = trackUri,
                deltas = deltas,
                name = name,
                type = trackType,
                startTimestamp = startTimestamp,
                endTimestamp = endTimestamp,
                trackedPeriod = trackedPeriod,
                distance = totalDistance,
                maxSpeed = maxMeterPerMilliSecondSpeed * 1000F,
                bounds = handler.bounds,
                waypoints = handler.waypoints)
    }

    fun distance(first: Waypoint, second: Waypoint, outArray: FloatArray): Float {
        Location.distanceBetween(first.latitude, first.longitude, second.latitude, second.longitude, outArray)

        return outArray[0]
    }
}
