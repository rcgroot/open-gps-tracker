package nl.renedegroot.android.opengpstrack.ng.summary.internal.databinding

import android.view.View
import android.widget.TextView
import androidx.databinding.BindingAdapter
import nl.renedegroot.android.gpstracker.utils.formatting.LocaleProvider
import nl.renedegroot.android.gpstracker.utils.formatting.StatisticsFormatter
import nl.renedegroot.android.gpstracker.utils.preference.UnitSettingsPreferences
import nl.renedegroot.android.gpstracker.utils.formatting.asSmallLetterSpans
import nl.renedegroot.android.opengpstrack.ng.summary.R

@BindingAdapter("immersive")
internal fun View.setImmersive(value: Boolean) {
    systemUiVisibility = if (value) {
        View.SYSTEM_UI_FLAG_IMMERSIVE or View.SYSTEM_UI_FLAG_FULLSCREEN or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
    } else {
        0
    }
}

@BindingAdapter("speed", "inverseSpeed")
internal fun TextView.setSpeed(speed: Float?, inverse: Boolean?) {
    text = if (speed == null || speed <= 0L) {
        context.getText(R.string.summary__empty_dash)
    } else {
        StatisticsFormatter(LocaleProvider(context), UnitSettingsPreferences(context)).convertMeterPerSecondsToSpeed(context, speed, inverse
                ?: false)
                .asSmallLetterSpans()

    }
}

@BindingAdapter("distance")
internal fun TextView.setDistance(distance: Float?) {
    text = if (distance == null || distance <= 0L) {
        context.getText(R.string.summary__empty_dash)
    } else {
        StatisticsFormatter(LocaleProvider(context), UnitSettingsPreferences(context)).convertMetersToCompactDistance(context, distance)
                .asSmallLetterSpans()
    }
}

@BindingAdapter("duration")
internal fun TextView.setDuration(duration: Long?) {
    text = if (duration == null || duration <= 0L) {
        context.getText(R.string.summary__empty_dash)
    } else {
        StatisticsFormatter(LocaleProvider(context), UnitSettingsPreferences(context)).convertSpanToCompactDuration(context, duration)
                .asSmallLetterSpans()
    }
}
