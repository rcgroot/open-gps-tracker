package nl.renedegroot.android.gpstracker.service.util

import android.net.Uri
import nl.renedegroot.android.gpstracker.ng.base.BaseConfiguration
import nl.renedegroot.android.gpstracker.ng.base.track.TrackTypeDescriptions
import nl.renedegroot.android.gpstracker.service.integration.ContentConstants
import nl.renedegroot.android.gpstracker.utils.contentprovider.getString
import nl.renedegroot.android.gpstracker.utils.contentprovider.runQuery


fun Uri.readTrackType(): TrackTypeDescriptions.TrackType {
    val trackId: Long = checkNotNull(lastPathSegment).toLong()
    val typeSelection = Pair("${ContentConstants.MetaDataColumns.KEY} = ?", listOf(TrackTypeDescriptions.KEY_META_FIELD_TRACK_TYPE))
    val contentType = metaDataTrackUri(trackId).runQuery(
            BaseConfiguration.baseComponent.contentResolver(),
            selectionPair = typeSelection) {
        it.getString(ContentConstants.MetaDataColumns.VALUE)
    }

    return TrackTypeDescriptions.trackTypeForContentType(contentType)
}

fun Uri.saveTrackType(trackType: TrackTypeDescriptions.TrackType) {
    val trackId: Long = checkNotNull(lastPathSegment).toLong()
    metaDataTrackUri(trackId).updateCreateMetaData(TrackTypeDescriptions.KEY_META_FIELD_TRACK_TYPE, trackType.contentValue)
}
