/*
 * Open GPS Tracker
 * Copyright (C) 2019 René de Groot
 *   
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses>.
 */

package nl.renedegroot.android.gpstracker.service.ng.internal

import android.location.Location
import nl.renedegroot.android.gpstracker.service.BuildConfig
import timber.log.Timber
import java.util.*
import kotlin.math.abs

/**
 * `mAcceptableAccuracy` indicates the maximum acceptable accuracy
 * of a waypoint in meters.
 */
private const val MAX_ACCURACY = 20f
/**
 * `MAX_REASONABLE_SPEED` is about 324 kilometer per hour or 201
 * mile per hour.
 */
private const val MAX_REASONABLE_SPEED = 90

/**
 * `MAX_REASONABLE_ALTITUDECHANGE` between the last few waypoints
 * and a new one the difference should be less then 200 meter.
 */
private const val MAX_REASONABLE_ALTITUDECHANGE = 200

internal class LocationFilter {

    private val weakLocations = Vector<Location>(3)
    private val altitudes = LinkedList<Double>()
    private var previousLocation: Location? = null

    fun filter(location: Location): Location? {
        if (BuildConfig.FLAVOR === "mock") {
            return location
        }

        // Do no include log wrong 0.0 lat 0.0 long, skip to next value in while-loop
        if (location.latitude == 0.0 || location.longitude == 0.0) {
            Timber.w("A wrong location was received, 0.0 latitude and 0.0 longitude... ")
            return null
        }

        // Do not log a waypoint which is more inaccurate then is configured to be acceptable
        if (location.accuracy > MAX_ACCURACY) {
            Timber.w("A weak location was received, lots of inaccuracy... (%f is more then max %f",
                    location.accuracy,
                    MAX_ACCURACY)
            return sanitize(addBadLocation(location))
        }

        // Do not log a waypoint which might be on any side of the previous waypoint
        previousLocation?.let {
            if (previousLocation != null && location.accuracy > it.distanceTo(location)) {
                Timber.w("A weak location was received, not quite clear from the previous waypoint... (%f more then max %f)",
                        it.accuracy, it.distanceTo(location))
                return sanitize(addBadLocation(location))
            }
        }

        // Speed checks, check if the proposed location could be reached from the previous one in sane speed
        // Common to jump on network logging and sometimes jumps on Samsung Galaxy S type of devices
        previousLocation?.let {
            // To avoid near instant teleportation on network location or glitches cause continent hopping
            val meters = location.distanceTo(previousLocation)
            val seconds = (location.time - it.time) / 1000L
            val speed = meters / seconds
            if (speed > MAX_REASONABLE_SPEED ) {
                Timber.w("A strange location was received, a really high speed of $speed m/s, prob wrong...")
                return sanitize(addBadLocation(location));
            }
        }

        return sanitize(location)
    }

    private fun sanitize(location: Location?): Location? =
            location?.let {
                // Remove speed if not sane
                if (location.speed > MAX_REASONABLE_SPEED) {
                    Timber.w("A strange speed, a really high speed, prob wrong...")
                    location.removeSpeed()
                }

                // Remove altitude if not sane
                if (location.hasAltitude()) {
                    if (!addSaneAltitude(location.altitude)) {
                        Timber.w("A strange altitude, a really big difference, prob wrong...")
                        location.removeAltitude()
                    }
                }
                // Older bad locations will not be needed
                weakLocations.clear()
                location
            }

    /**
     * Store a bad location, when to many bad locations are stored the the
     * storage is cleared and the least bad one is returned
     *
     * @param location bad location
     * @return null when the bad location is stored or the least bad one if the
     * storage was full
     */
    private fun addBadLocation(location: Location?): Location? {
        weakLocations.add(location)
        return if (weakLocations.size < 3) {
            null
        } else {
            var best = weakLocations.lastElement()
            for (wimp in weakLocations) {
                if (wimp.hasAccuracy() && best.hasAccuracy() && wimp.accuracy < best.accuracy) {
                    best = wimp
                } else {
                    if (wimp.hasAccuracy() && !best.hasAccuracy()) {
                        best = wimp
                    }
                }
            }
            synchronized(weakLocations) {
                weakLocations.clear()
            }
            best
        }
    }

    /**
     * Builds a bit of knowledge about altitudes to expect and return if the
     * added value is deemed sane.
     *
     * @param altitude
     * @return whether the altitude is considered sane
     */
    private fun addSaneAltitude(altitude: Double): Boolean {
        val sane: Boolean
        var avg = 0.0
        var elements = 0
        // Even insane altitude shifts increases alter perception
        altitudes.add(altitude)
        if (altitudes.size > 3) {
            altitudes.poll()
        }
        for (alt in altitudes) {
            avg += alt
            elements++
        }
        avg /= elements
        sane = abs(altitude - avg) < MAX_REASONABLE_ALTITUDECHANGE

        return sane
    }
}
