<?xml version="1.0" encoding="utf-8"?><!--
  ~ Open GPS Tracker
  ~ Copyright (C) 2018  René de Groot
  ~
  ~ This program is free software: you can redistribute it and/or modify
  ~ it under the terms of the GNU General Public License as published by
  ~ the Free Software Foundation, either version 3 of the License, or
  ~ (at your option) any later version.
  ~
  ~ This program is distributed in the hope that it will be useful,
  ~  but WITHOUT ANY WARRANTY; without even the implied warranty of
  ~  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  ~  GNU General Public License for more details.
  ~
  ~  You should have received a copy of the GNU General Public License
  ~  along with this program. If not, see <http://www.gnu.org/licenses/>.
  -->
<layout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools">

    <data>

        <import type="android.view.View" />

        <variable
            name="presenter"
            type="nl.renedegroot.opengpstracker.exporter.internal.ExportPresenter" />

        <variable
            name="model"
            type="nl.renedegroot.opengpstracker.exporter.internal.ExportViewModel" />
    </data>

    <androidx.constraintlayout.widget.ConstraintLayout
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:paddingLeft="@dimen/exporter__activity_horizontal_margin"
        android:paddingTop="@dimen/exporter__activity_horizontal_margin"
        android:paddingRight="@dimen/exporter__activity_horizontal_margin"
        android:paddingBottom="@dimen/exporter__activity_horizontal_margin">

        <TextView
            android:id="@+id/exporter__fragment_export_explained"
            android:layout_width="0dp"
            android:layout_height="wrap_content"
            android:layout_marginStart="8dp"
            android:layout_marginTop="8dp"
            android:layout_marginEnd="8dp"
            android:text="@string/exporter__export_explained"
            android:textAppearance="@style/TextAppearance.MaterialComponents.Subtitle1"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toTopOf="parent" />

        <ImageView
            android:id="@+id/exporter__from"
            android:layout_width="0dp"
            android:layout_height="wrap_content"
            android:layout_marginStart="8dp"
            android:layout_marginTop="8dp"
            android:layout_marginEnd="8dp"
            android:layout_marginBottom="8dp"
            android:focusable="false"
            app:layout_constraintEnd_toStartOf="@+id/exporter__arrow"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toBottomOf="@+id/exporter__fragment_export_explained"
            app:srcCompat="@drawable/exporter__app_icon" />

        <ImageView
            android:id="@+id/exporter__arrow"
            android:layout_width="30dp"
            android:layout_height="0dp"
            android:layout_marginStart="8dp"
            android:layout_marginEnd="8dp"
            android:focusable="false"
            app:layout_constraintBottom_toBottomOf="@+id/exporter__from"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toTopOf="@+id/exporter__from"
            app:srcCompat="@drawable/exporter__ic_arrow_forward_black_24dp" />

        <ImageView
            android:id="@+id/exporter__to"
            android:layout_width="0dp"
            android:layout_height="0dp"
            android:layout_marginStart="8dp"
            android:layout_marginEnd="8dp"
            android:contentDescription="@string/exporter__drive"
            android:focusable="false"
            android:onClick="@{(view) -> presenter.onConnectClicked()}"
            android:scaleType="centerInside"
            app:layout_constraintBottom_toBottomOf="@+id/exporter__from"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintStart_toEndOf="@+id/exporter__arrow"
            app:layout_constraintTop_toTopOf="@+id/exporter__from"
            app:layout_constraintVertical_bias="1.0"
            app:srcCompat="@{model.selectedTarget}" />

        <Spinner
            android:id="@+id/exporter__sink_selection"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:onItemSelected="@{(p, v, pos, id) -> presenter.onTargetSelected(model.exportTargets[pos])}"
            app:adapter="@{model.exportTargets}"
            app:layout_constraintEnd_toEndOf="@+id/exporter__to"
            app:layout_constraintTop_toBottomOf="@+id/exporter__to" />

        <CheckBox
            android:id="@+id/exporter__fragment_export_drive"
            android:layout_width="0dp"
            android:layout_height="wrap_content"
            android:layout_marginEnd="8dp"
            android:onClick="@{(view) -> presenter.onConnectClicked()}"
            android:text="@string/exporter__export_drive_connected"
            android:textAppearance="@style/TextAppearance.AppCompat"
            app:driveCheckbox="@{model.status}"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintStart_toStartOf="@+id/exporter__from"
            app:layout_constraintTop_toBottomOf="@+id/exporter__sink_selection" />

        <TextView
            android:id="@+id/exporter__fragment_export_progress_tracks"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:layout_marginStart="8dp"
            android:layout_marginTop="8dp"
            android:layout_marginEnd="8dp"
            android:paddingStart="8dp"
            android:paddingEnd="8dp"
            android:text="@{@string/exporter__export_tracks(model.completedTracks, model.totalTracks)}"
            android:textAppearance="@android:style/TextAppearance.Material"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toBottomOf="@+id/exporter__fragment_export_drive"
            app:visible_after_start="@{model.status}"
            tools:text="Tracks: 1 out of 12" />

        <TextView
            android:id="@+id/exporter__fragment_export_progress_waypoints"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:layout_marginStart="8dp"
            android:layout_marginEnd="8dp"
            android:enabled="true"
            android:paddingStart="8dp"
            android:paddingEnd="8dp"
            android:text="@{@string/exporter__export_waypoints(model.completedWaypoints, model.totalWaypoints)}"
            android:textAppearance="@android:style/TextAppearance.Material"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toBottomOf="@+id/exporter__fragment_export_progress_tracks"
            app:visible_after_start="@{model.status}"
            tools:text="Waypoints: 1 out of 34" />

        <ProgressBar
            android:id="@+id/exporter__fragment_export_progress_bar"
            style="@style/Base.Widget.AppCompat.ProgressBar.Horizontal"
            android:layout_width="0dp"
            android:layout_height="wrap_content"
            android:layout_gravity="end"
            android:layout_marginStart="8dp"
            android:layout_marginTop="8dp"
            android:layout_marginEnd="8dp"
            android:keepScreenOn="true"
            android:paddingLeft="8dp"
            android:paddingRight="8dp"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toBottomOf="@+id/exporter__fragment_export_progress_waypoints"
            app:visible_during_run="@{model.status}" />

        <Button
            android:id="@+id/exporter__fragment_export_nextStep"
            android:layout_width="0dp"
            android:layout_height="39dp"
            android:layout_alignParentBottom="true"
            android:layout_marginEnd="8dp"
            android:layout_marginBottom="8dp"
            android:onClick="@{(view) -> presenter.onNextStepClicked(model.status)}"
            app:button_status="@{model.status}"
            app:layout_constraintBottom_toBottomOf="parent"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintHorizontal_bias="0.471"
            app:layout_constraintStart_toStartOf="parent"
            tools:text="Next step" />

        <ImageView
            android:id="@+id/exporter__imageview"
            android:layout_width="0dp"
            android:layout_height="0dp"
            android:layout_marginStart="8dp"
            android:layout_marginTop="8dp"
            android:layout_marginEnd="8dp"
            android:layout_marginBottom="8dp"
            android:visibility="@{model.completedTracks==model.totalTracks ? View.VISIBLE : View.GONE}"
            app:layout_constraintBottom_toTopOf="@+id/exporter__fragment_export_nextStep"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toBottomOf="@+id/exporter__fragment_export_progress_waypoints"
            app:srcCompat="@drawable/exporter__ic_check_black_24dp"
            app:tint="@color/exporter__green_light" />

    </androidx.constraintlayout.widget.ConstraintLayout>
</layout>
