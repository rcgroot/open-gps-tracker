/*
 * Open GPS Tracker
 * Copyright (C) 2020  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package nl.renedegroot.android.gpstracker.ng.base.permissions

import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import androidx.core.os.bundleOf
import nl.renedegroot.android.gpstracker.ng.base.R


const val ARGUMENT_PERMISSIONS = "ARGUMENT_PERMISSIONS"
const val ARGUMENT_INTENT = "ARGUMENT_INTENT"

fun permissionActivityIntent(
        context: Context,
        permissions: List<String>,
        pendingIntent: PendingIntent? = null
) =
        Intent(context.getString(R.string.base__permission_action)).also {
            it.putExtras(bundleOf(
                    ARGUMENT_PERMISSIONS to permissions,
                    ARGUMENT_INTENT to pendingIntent
            ))
        }