/*
 * Open GPS Tracker
 * Copyright (C) 2018  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package nl.renedegroot.android.gpstracker.v2.sharedwear.datasync

import android.content.Context
import com.google.android.gms.wearable.*
import nl.renedegroot.android.gpstracker.v2.sharedwear.model.StatisticsMessage
import nl.renedegroot.android.gpstracker.v2.sharedwear.model.StatusMessage
import nl.renedegroot.android.gpstracker.v2.sharedwear.model.WearMessage


class DataSender(
        private val context: Context) :
        DataClient.OnDataChangedListener {

    private var updateListener: DataUpdateListener? = null

    fun updateMessage(message: WearMessage, urgent: Boolean = false) {
        val putDataMapReq = PutDataMapRequest.create(message.path)
        putDataMapReq.dataMap.putAll(message.toDataMap())
        if (urgent) {
            putDataMapReq.setUrgent()
        }
        val putDataReq = putDataMapReq.asPutDataRequest()
        Wearable.getDataClient(context).putDataItem(putDataReq)
    }

    fun start(updateListener: DataUpdateListener) {
        this.updateListener = updateListener
        startDataListener()
    }

    fun stop() {
        stopDataListener()
    }

    override fun onDataChanged(dataEvents: DataEventBuffer) {
        dataEvents.forEach {
            when (it.dataItem.uri.path) {
                StatusMessage.PATH_STATUS -> {
                    val status = StatusMessage(DataMapItem.fromDataItem(it.dataItem).dataMap)
                    updateListener?.onStatusUpdate(status)
                }
                StatisticsMessage.PATH_STATISTICS -> {
                    val status = StatisticsMessage(DataMapItem.fromDataItem(it.dataItem).dataMap)
                    updateListener?.onStatisticsUpdate(status)
                }
            }
        }
    }

    private fun startDataListener() {
        Wearable.getDataClient(context).addListener(this)

    }

    private fun stopDataListener() {
        Wearable.getDataClient(context).removeListener(this)
    }

    interface DataUpdateListener {
        fun onStatusUpdate(status: StatusMessage)
        fun onStatisticsUpdate(status: StatisticsMessage)

    }

}
