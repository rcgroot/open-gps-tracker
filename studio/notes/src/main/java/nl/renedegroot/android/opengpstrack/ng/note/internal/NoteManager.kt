/*
 * Open GPS Tracker
 * Copyright (C) 2019 René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses>.
 */

package nl.renedegroot.android.opengpstrack.ng.note.internal

import android.content.ContentValues
import android.content.Context
import android.net.Uri
import androidx.core.content.contentValuesOf
import androidx.core.net.toFile
import nl.renedegroot.android.gpstracker.ng.base.BuildConfig
import nl.renedegroot.android.gpstracker.ng.base.location.LatLng
import nl.renedegroot.android.gpstracker.service.db.DatabaseConstants.Media.TRACK
import nl.renedegroot.android.gpstracker.service.integration.ContentConstants
import nl.renedegroot.android.gpstracker.service.integration.ContentConstants.Media.MEDIA
import nl.renedegroot.android.gpstracker.service.integration.ContentConstants.Media.URI
import nl.renedegroot.android.gpstracker.service.integration.ContentConstants.Waypoints.*
import nl.renedegroot.android.gpstracker.service.util.Waypoint
import nl.renedegroot.android.gpstracker.service.util.waypointUri
import nl.renedegroot.android.gpstracker.utils.contentprovider.append
import nl.renedegroot.android.gpstracker.utils.contentprovider.getLong
import nl.renedegroot.android.gpstracker.utils.contentprovider.map
import nl.renedegroot.android.gpstracker.utils.contentprovider.runQuery
import nl.renedegroot.android.gpstracker.utils.file.filesDir
import nl.renedegroot.android.gpstracker.utils.test.withResources
import java.io.File
import java.io.FileReader
import java.io.FileWriter
import java.util.*

class NoteManager(private val context: Context) {

    fun createImageNote(inputFile: File): Uri {
        val c = Calendar.getInstance()
        val newName = String.format("Imagenote_%tY-%tm-%td_%tH%tM%tS.jpg", c, c, c, c, c, c)
        val file = context.filesDir("notes", newName)
        inputFile.copyTo(file, overwrite = true)

        return Uri.fromFile(file)
    }

    fun createTextNote(noteText: String): Uri {
        val c = Calendar.getInstance()
        val newName = String.format("Textnote_%tY-%tm-%td_%tH%tM%tS.txt", c, c, c, c, c, c)
        val file = context.filesDir("notes", newName)
        withResources {
            file.parentFile.mkdirs()
            file.createNewFile()
            val fileWriter = FileWriter(file).use()
            fileWriter.append(noteText)
            fileWriter.flush()
        }

        return Uri.fromFile(file)
    }

    fun saveTitleNote(title: String, waypoint: Uri) {
        val uriEncodedTitle = Uri.Builder()
                .scheme("content")
                .authority("${BuildConfig.providerAuthority}.string")
                .appendPath(title)
                .build()
                .toString()
        val values = contentValues { put(URI, uriEncodedTitle) }
        context.contentResolver.insert(waypoint.append(MEDIA), values)
    }

    fun loadTitleNote(waypoint: Uri): String? =
            waypoint.append(MEDIA).runQuery(
                    context.contentResolver,
                    projection = listOf(URI),
                    selectionPair = ("$URI LIKE 'content://${BuildConfig.providerAuthority}.string%'") to emptyList()) {
                Uri.parse(it.getString(0)).path?.trim('/')
            }


    fun saveImageNote(imageNote: Uri, waypoint: Uri) {
        val values = contentValuesOf(URI to imageNote.toString())
        context.contentResolver.insert(waypoint.append(MEDIA), values)
    }

    fun loadImageNote(waypoint: Uri): Uri? =
            waypoint.append(MEDIA).runQuery(
                    context.contentResolver,
                    projection = listOf(URI),
                    selectionPair = ("$URI LIKE 'file://%.jpg'") to emptyList()) { cursor ->
                cursor.getString(0)?.let { value ->
                    Uri.parse(value)
                }
            }

    fun saveTextNote(textNote: Uri, waypoint: Uri) {
        val values = contentValues { put(URI, textNote.toString()) }
        context.contentResolver.insert(waypoint.append(MEDIA), values)
    }

    fun loadTextNote(waypoint: Uri): String? =
            waypoint.append(MEDIA).runQuery(
                    context.contentResolver,
                    projection = listOf(URI),
                    selectionPair = ("$URI LIKE 'file://%.txt'") to emptyList()) { cursor ->
                cursor.getString(0)?.let { value ->
                    val uri = Uri.parse(value)
                    withResources {
                        FileReader(uri.toFile())
                                .use()
                                .readText()
                    }
                }
            }

    fun listNotes(mediaUri: Uri): List<Pair<Uri, LatLng>> {
        return mediaUri.map(context.contentResolver) { cursor ->
            val trackId = checkNotNull(cursor.getLong(TRACK))
            val segmentId = checkNotNull(cursor.getLong(ContentConstants.MediaColumns.SEGMENT))
            val waypointId = checkNotNull(cursor.getLong(ContentConstants.MediaColumns.WAYPOINT))
            val uri = waypointUri(trackId, segmentId, waypointId)
            uri.runQuery(context.contentResolver, projection = listOf(LATITUDE, LONGITUDE)) {
                uri to LatLng(it.getDouble(0), it.getDouble(1))
            }
        }.filterNotNull().distinct()
    }
}

private fun contentValues(block: ContentValues.() -> Unit) = ContentValues().apply { block() }
