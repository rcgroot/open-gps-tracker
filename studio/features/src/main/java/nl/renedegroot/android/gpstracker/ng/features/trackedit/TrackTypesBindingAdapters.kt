/*
 * Open GPS Tracker
 * Copyright (C) 2018  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package nl.renedegroot.android.gpstracker.ng.features.trackedit

import android.widget.Spinner
import androidx.appcompat.widget.AppCompatSpinner
import androidx.databinding.BindingAdapter
import nl.renedegroot.android.gpstracker.ng.base.track.TrackTypeDescriptions

@BindingAdapter("trackTypes", "selection", requireAll = false)
fun Spinner.setTrackTypes(trackTypes: List<TrackTypeDescriptions.TrackType>?, selection: Int?) {
    val viewAdapter: TrackTypeSpinnerAdapter
    if (adapter is TrackTypeSpinnerAdapter) {
        viewAdapter = adapter as TrackTypeSpinnerAdapter
        viewAdapter.trackTypes = trackTypes ?: listOf()
    } else {
        viewAdapter = TrackTypeSpinnerAdapter(context, trackTypes ?: listOf())
        adapter = viewAdapter
    }

    if (selection != null) {
        setSelection(selection)
    } else {
        setSelection(AppCompatSpinner.INVALID_POSITION)
    }
}
