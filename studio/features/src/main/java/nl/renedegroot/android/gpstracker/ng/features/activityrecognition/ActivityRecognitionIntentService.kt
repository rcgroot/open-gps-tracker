/*
 * Open GPS Tracker
 * Copyright (C) 2018  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package nl.renedegroot.android.gpstracker.ng.features.activityrecognition

import android.app.IntentService
import android.content.Context
import android.content.Intent
import android.net.Uri
import com.google.android.gms.location.ActivityTransition.ACTIVITY_TRANSITION_ENTER
import com.google.android.gms.location.ActivityTransition.ACTIVITY_TRANSITION_EXIT
import com.google.android.gms.location.ActivityTransitionEvent
import com.google.android.gms.location.ActivityTransitionResult
import com.google.android.gms.location.DetectedActivity
import nl.renedegroot.android.gpstracker.ng.base.track.TrackTypeDescriptions
import nl.renedegroot.android.gpstracker.ng.base.track.TrackTypeDescriptions.Companion.VALUE_TYPE_BIKE
import nl.renedegroot.android.gpstracker.ng.base.track.TrackTypeDescriptions.Companion.VALUE_TYPE_CAR
import nl.renedegroot.android.gpstracker.ng.base.track.TrackTypeDescriptions.Companion.VALUE_TYPE_DEFAULT
import nl.renedegroot.android.gpstracker.ng.base.track.TrackTypeDescriptions.Companion.VALUE_TYPE_RUN
import nl.renedegroot.android.gpstracker.ng.base.track.TrackTypeDescriptions.Companion.VALUE_TYPE_WALK
import nl.renedegroot.android.gpstracker.ng.base.track.TrackTypeDescriptions.Companion.trackTypeForContentType
import nl.renedegroot.android.gpstracker.service.integration.ContentConstants.TracksColumns.CREATION_TIME
import nl.renedegroot.android.gpstracker.service.util.saveTrackType
import nl.renedegroot.android.gpstracker.utils.contentprovider.runQuery

private const val EXTRA_TRACK_URI = "EXTRA_TRACK_URI"
private const val FIVE_MINUTES_MS = 5 * 60 * 1000L

fun recognitionIntent(context: Context, trackUri: Uri) =
        Intent(context, ActivityRecognitionIntentService::class.java).apply {
            putExtra(EXTRA_TRACK_URI, trackUri)
        }

class ActivityRecognitionIntentService : IntentService("ActivityRecognitionIntentService") {

    override fun onHandleIntent(intent: Intent?) {
        val result = ActivityTransitionResult.extractResult(intent)
        val trackUri = intent?.getParcelableExtra<Uri>(EXTRA_TRACK_URI)
        if (result != null && trackUri != null) {
            val creationTime = trackUri.runQuery(
                    contentResolver,
                    projection = listOf(CREATION_TIME)
            ) { it.getLong(0) } ?: FIVE_MINUTES_MS
            val trackElapsedRealTimeNanos = (System.currentTimeMillis() - creationTime) * 1_000_000L
            val trackType = trackTypeForTransitions(result.transitionEvents, trackElapsedRealTimeNanos)
            trackType?.let { trackUri.saveTrackType(it) }
        }
    }

    fun trackTypeForTransitions(events: List<ActivityTransitionEvent>, trackElapsedRealTimeNanos: Long): TrackTypeDescriptions.TrackType? {
        if (events.isEmpty()) {
            return null
        }
        val activityType = events
                .asSequence()
                .filter { it.transitionType == ACTIVITY_TRANSITION_ENTER }
                .filter { it.elapsedRealTimeNanos < trackElapsedRealTimeNanos }
                .plus(ActivityTransitionEvent(events.last().activityType, ACTIVITY_TRANSITION_EXIT, 0))
                .zipWithNext { begin, end -> begin.elapsedRealTimeNanos - end.elapsedRealTimeNanos to begin.activityType }
                .maxBy { it.first }
                ?.second

        return when (activityType) {
            DetectedActivity.WALKING -> trackTypeForContentType(VALUE_TYPE_WALK)
            DetectedActivity.RUNNING -> trackTypeForContentType(VALUE_TYPE_RUN)
            DetectedActivity.ON_BICYCLE -> trackTypeForContentType(VALUE_TYPE_BIKE)
            DetectedActivity.IN_VEHICLE -> trackTypeForContentType(VALUE_TYPE_CAR)
            else -> trackTypeForContentType(VALUE_TYPE_DEFAULT)
        }
    }
}
