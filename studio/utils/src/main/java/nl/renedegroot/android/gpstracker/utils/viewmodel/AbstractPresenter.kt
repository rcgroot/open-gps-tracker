/*
 * Open GPS Tracker
 * Copyright (C) 2018  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package nl.renedegroot.android.gpstracker.utils.viewmodel

import androidx.annotation.CallSuper
import androidx.annotation.WorkerThread
import kotlinx.coroutines.*
import nl.renedegroot.android.gpstracker.ng.base.BaseConfiguration
import timber.log.Timber

/**
 * Basis for a long lived (@see android.arch.lifecycle.ViewModel) Presenter that
 * can start / stop based on visibility to the user.
 *
 * Instantiate using the android.arch.lifecycle.ViewModel factory means.
 */
abstract class AbstractPresenter : CoroutineViewModel() {
    private val changeDispatcher: CoroutineDispatcher = BaseConfiguration.baseComponent.uiUpdateExecutor().asCoroutineDispatcher()
    private var firstStart = true
    private var started = false
    private var dirty = true

    /**
     * Called when the Presenter is visible to the user. Will call onChange() on initial
     * start and when markDirty() was called when not started.
     */
    fun start() {
        launch(Dispatchers.Main + NonCancellable) {
            if (!started) {
                started = true
                if (firstStart) {
                    firstStart = false
                    onFirstStart()
                }
                onStart()
            }
            withContext(changeDispatcher) {
                checkUpdate()
            }
        }
    }

    /**
     * Call when the Presenter is no longer started.
     */
    fun stop() {
        launch(Dispatchers.Main + NonCancellable) {
            if (started) {
                started = false
                onStop()
            }
        }
    }

    /**
     * Mark the Presenter as dirty when the update to the View would incur CPU, memory or other
     * load not useful when the presenter is started.
     */
    protected fun markDirty() {
        launch(changeDispatcher) {
            dirty = true
            checkUpdate()
        }
    }

    /**
     * Called before right before the first time that @see onStart is called
     */
    open suspend fun onFirstStart() {
    }

    /**
     * Override to start listening to model changes which incur load on the device. Such as
     * observing sensors or polling components.
     */
    open suspend fun onStart() {
    }

    /**
     * Execute View updates deferred by using markDirty. Will run on a worker thread to allow
     * IO.
     */
    abstract suspend fun onChange()

    /**
     * Override to stop listening to model changes.
     */
    open suspend fun onStop() {
    }

    private suspend fun checkUpdate() {
        if (dirty && started) {
            dirty = false
            onChange()
        }
    }
}
