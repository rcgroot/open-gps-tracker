/*
 * Open GPS Tracker
 * Copyright (C) 2019 René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses>.
 */

package nl.renedegroot.android.gpstracker.utils.preference

import android.content.Context
import android.content.SharedPreferences
import androidx.core.content.edit
import kotlin.reflect.KProperty

fun intPreference(context: Context, preferenceName: String, key: String, defaultValue: Int) =
        IntPreferenceDelegate(context, preferenceName, key, defaultValue)

class IntPreferenceDelegate(
        context: Context,
        preferenceName: String,
        key: String,
        defaultValue: Int
) {

    private val preference by lazy {
        IntPreference(
                context.getSharedPreferences(preferenceName, Context.MODE_PRIVATE),
                key,
                defaultValue
        )
    }

    operator fun getValue(thisRef: Any?, property: KProperty<*>): Int? =
            preference.read()

    operator fun setValue(thisRef: Any?, property: KProperty<*>, value: Int?) {
        if (value != null) {
            preference.write(value)
        } else {
            preference.clear()
        }
    }
}

private class IntPreference(
        private val preferences: SharedPreferences,
        private val key: String,
        private val defaultValue: Int
) {

    fun read(): Int =
            preferences.getInt(key, defaultValue)

    fun write(value: Int) =
            preferences.edit { putInt(key, value) }

    fun clear() =
            preferences.edit { remove(key) }

}
