# open-gpstracker-ng
An application that can track your travels of your Android by storing your GPS locations. Draws the route real-time on Google maps. Uses coloring to render speeds.

## Screenshot

![Track](resources/screenshots/001%20TrackScreen.png "Track")